public class Kunde {
	private int kundennummer;
	private String name;
	
	public Kunde() {
		this.kundennummer = 0;
		this.name = "Unbekannt";
	}	
	
	public Kunde(int knummer , String name) {	
		this.kundennummer = knummer ;
		this.name = name;
	
	}

	public int getKundennummer() {
		return kundennummer;
	}

	public void setKundennummer(int kundennummer) {
		this.kundennummer = kundennummer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
        return "Name: " + this.name + ", Kundenummer: " + this.kundennummer; 

	}

}