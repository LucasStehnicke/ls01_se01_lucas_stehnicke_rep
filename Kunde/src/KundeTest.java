public class KundeTest {

	public static void main(String[] args) {
		
		Kunde k1 = new Kunde();
		k1.setName("Heinz");
		k1.setKundennummer(15);
		
		Kunde k2 = new Kunde();
		k2.setName("Karl");
		k2.setKundennummer(865);
		
		System.out.println(k1);
		System.out.println(k2);
	}

}
