import java.util.Scanner;

class Fahrkartenautomat
{
	public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
                         
       double zuZahlenderBetrag = fahrkartenbestellungErfassenBetrag();
       double zuZahlenderBetragGesamt = fahrkartenbestellungErfassenAnzahl(zuZahlenderBetrag);
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetragGesamt);
       fahrkartenAusgeben();
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetragGesamt);
      
    }
	public static double fahrkartenbestellungErfassenBetrag() {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = 0;
		double wunsch;
		double zahl;
		
		System.out.println("Fahrkartenbestellvorgang");
		System.out.println("------------------------");
		System.out.println();
		System.out.println("W�hlen Sie Ihre Fahrkarte f�r Berlin aus: ");
		System.out.println("(1) Berlin Tarifbereich AB");
		System.out.println("(2) Berlin Tarifbereich BC");
		System.out.println("(3) Berlin Tarifbereich ABC");
		
	    wunsch = tastatur.nextDouble();
		if (wunsch == 1) {
			System.out.println("W�hlen Sie Ihre Tarifstufe f�r den Bereich Berlin AB aus: ");
			System.out.println("(1) Regeltarif: 2,90�");
			System.out.println("(2) Erm��igt: 1,80�");
			
			zahl = tastatur.nextDouble();
			
			if(zahl == 1) {
				zuZahlenderBetrag += 2.90;
			}
			if(zahl == 2) {
				zuZahlenderBetrag += 1.80;
			}
			if(zahl < 1 || zahl > 2) {
				System.out.println("Geben Sie bitte eine Option ein, die Ihnen zur Auswahl steht.");
				
			}
		}
		if (wunsch == 2) {
			System.out.println("W�hlen Sie Ihre Tarifstufe f�r den Bereich Berlin BC aus: ");
			System.out.println("(1) Regeltarif: 3,30�");
			System.out.println("(2) Erm��igt: 2,30�");
			
			zahl = tastatur.nextDouble();
			
			if(zahl == 1) {
				zuZahlenderBetrag += 3.30;
			}
			if(zahl == 2) {
				zuZahlenderBetrag += 2.30;
			}
			if(zahl < 1 || zahl > 2) {
				System.out.println("Geben Sie bitte eine Option ein, die Ihnen zur Auswahl steht.");
			}
		}
		if (wunsch == 3) {
			System.out.println("W�hlen Sie Ihre Tarifstufe f�r den Bereich Berlin ABC aus: ");
			System.out.println("(1) Regeltarif: 3,60�");
			System.out.println("(2) Erm��igt: 2,30�");
			
			zahl = tastatur.nextDouble();
			
			if(zahl == 1) {
				zuZahlenderBetrag += 3.60;
			}
			if(zahl == 2) {
				zuZahlenderBetrag += 2.30;
			}
			if(zahl < 1 || zahl > 2) {
				System.out.println("Geben Sie bitte eine Option ein, die Ihnen zur Auswahl steht.");
			}
		}
		if (wunsch < 1 || wunsch > 3) {
			System.out.println("Geben Sie bitte eine Option ein, die Ihnen zur Auswahl steht.");
			System.out.println();
			fahrkartenbestellungErfassenBetrag();
		}
	    	    	    
	    return zuZahlenderBetrag;
	}
		
	public static double fahrkartenbestellungErfassenAnzahl(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double anzahlFahrkarten;
		
		System.out.print("Anzahl der Tickets: ");
		anzahlFahrkarten = tastatur.nextByte();
		
		if (anzahlFahrkarten > 10 || anzahlFahrkarten < 1) {
			anzahlFahrkarten = 1;
			System.out.println("Sie haben eine unzul�ssige Fahrkartenanzahl eingegeben. Es wird mit dem Standardwert 1  weitergerechnet.");
		}
			
		return anzahlFahrkarten * zuZahlenderBetrag;
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetragGesamt) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMuenze;
		
	    while(zuZahlenderBetragGesamt >= eingezahlterGesamtbetrag)
	      {
	       System.out.printf("%s%.2f%s%n", "Noch zu zahlen: ", (zuZahlenderBetragGesamt - eingezahlterGesamtbetrag),  " Euro");
	       System.out.print("Eingabe (mind. 5 Ct, h�chstens 2 Euro): ");
	       eingeworfeneMuenze = tastatur.nextDouble();
	       eingezahlterGesamtbetrag += eingeworfeneMuenze;
	      }
	    return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetragGesamt) {
				
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetragGesamt;
	       if(rueckgabebetrag > 0.0)
	       {
	    	   System.out.printf("%s%.2f%s%n", "Der R�ckgabebetrag in H�he von ", rueckgabebetrag, " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2,00 EURO");
		          rueckgabebetrag -= 2.0;
	           }
	           while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1,00 EURO");
		          rueckgabebetrag -= 1.0;
	           }
	           while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          rueckgabebetrag -= 0.5;
	           }
	           while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgabebetrag -= 0.2;
	           }
	           while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgabebetrag -= 0.1;
	           }
	           while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgabebetrag -= 0.05;
	           }
	           while(rueckgabebetrag >= 0.02)// 2 CENT-M�nzen
	           {
	        	  System.out.println("2 CENT");
	 	          rueckgabebetrag -= 0.02;
	           }
	           while(rueckgabebetrag >= 0.01)// 1 CENT-M�nzen
	           {
	        	  System.out.println("1 CENT");
	 	          rueckgabebetrag -= 0.01;
	           }
	           
	       }
	     
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	         
	}
	  
		
	}

