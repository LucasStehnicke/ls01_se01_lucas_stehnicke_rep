import java.util.Scanner;

public class QuersummeWhile {

        public static void main(String[] args) {

            Scanner scan = new Scanner(System.in);

            int Zahl = 0;
            int Quersumme  = 0;

            System.out.printf("Zahl eingeben: ");
            Zahl = scan.nextInt();

            while (Zahl != 0) {
                Quersumme += Zahl % 10;
                Zahl = Zahl / 10;
            }
                System.out.printf("Die Quersumme von %d ist %d", Zahl, Quersumme);
        }
    }