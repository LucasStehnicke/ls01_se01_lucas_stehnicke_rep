﻿/* Operatoren.java
   Uebung zu Operatoren in Java
   @author
   @version
*/
public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/
	  
	  int zahl1, zahl2;
	  
    System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */

    zahl1 = 75;
    zahl2 = 23;
    
    System.out.println("Zahl1: " + zahl1 );
    System.out.println("Zahl2: " + zahl2 );
    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */

    int erg = zahl1 + zahl2;
    System.out.println("Zahl1 + Zahl2 = " + erg );
    
    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */
    int erg2 = zahl1 - zahl2;
    System.out.println("Zahl1 - Zahl2 = " + erg2);
    int erg3 = zahl1 * zahl2;
    System.out.println("Zahl1 * Zahl2 = " + erg3);
    int erg4 = zahl1 / zahl2;
    System.out.println("Zahl1 / Zahl2 = " + erg4);
    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */

    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */

    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
          
  }//main
}// Operatoren
