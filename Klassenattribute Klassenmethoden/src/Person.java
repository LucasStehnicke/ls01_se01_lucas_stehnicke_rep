public class Person {
    private String name;
    private int alter;

    public Person() {
        this.name = "Unbekannt";
        this.alter = 0;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAlter() {
        return alter;
    }
    public void setAlter(int alter) {
        this.alter = alter;
    }
    public String toString() {
        return "Name: " + this.name + ", Alter:" + this.alter; 
    }
}

