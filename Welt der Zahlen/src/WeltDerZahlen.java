/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie duerfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 9 ;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne  = 200000000000l ;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3769000 ; 
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 7334 ; 
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int   gewichtKilogramm = 190000 ;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int   flaecheGroessteLand = 17098242 ;
    
    // Wie groß ist das kleinste Land der Erde?
    
    int  flaecheKleinsteLand = 2 ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Bewohner Berlins: " + bewohnerBerlin);
    
    System.out.println("Alter in Tagen: " + alterTage + " Tage");
    
    System.out.println("Schwerstes Tier der Welt wiegt: " + gewichtKilogramm + " kg");
    
    System.out.println("Fl�che des gr��ten Landes: "+ flaecheGroessteLand + " km�");
    
    System.out.println("Fl�che des kleinsten Landes: " + flaecheKleinsteLand + " km�");
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

